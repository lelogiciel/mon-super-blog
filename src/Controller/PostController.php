<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;


class PostController extends AbstractController{
    

    #[Route('/article', name:'article_page')]
    public function article_page(EntityManagerInterface $manager)
    {
        $posts = $manager->getRepository(Post::class)->findAll();
        return $this->render('article/article-page.html.twig', ['posts' => $posts]);
    }


    #[Route('/article/add', name:'add_article')]
    public function add_article(Request $request, EntityManagerInterface $manager)
    { // injecton de dependances pour pouvoir utiliser manager et request
        
        if($request->isMethod('POST')){
            $title   = $request->request->get('title');
            $content = $request->request->get('content');
            $publish = $request->request->get('publish');
            $post = new Post();
            $post->setTitle($title);
            $post->setContent($content);

            if($publish == "on"){
                $post->setPublishedAt(new \DatetimeImmutable());
            }

            $manager->persist($post);
            $manager->flush(); // s'occupe de l'ecriture en bdd
            $this->addFlash('success', 'Votre article a été bien ajouté');
            return $this->redirectToRoute('article_page');
        }

        return $this->render('article/add-article.html.twig');
        
    }


    #[Route('/article/update/{id}', name: 'update_article')]
    public function update_article(Request $request, EntityManagerInterface $manager, int $id): Response
    {
        $post = $manager->getRepository(Post::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                "il n'existe pas de categorie pour cet id ".$id
            );
        }
        
        if($request->isMethod('POST')){
            
            $title = $request->request->get('title');
            $content = $request->request->get('content');
            $publish = $request->request->get('publish');
            $post->setTitle($title);
            $post->setContent($content);
            if($publish == "on"){
                $post->setPublishedAt(new \DatetimeImmutable());
            }
            else{
                $post->setPublishedAt(null);
            }
            $manager->flush();
            $this->addFlash('success', 'Votre article a été bien modifié');
            return $this->redirectToRoute('article_page');

        }
        return $this->render('article/update-article.html.twig', ['post' => $post]);


    }


    #[Route('/article/delete/{id}', name: 'delete_article')]
    public function delete_article(Request $request, EntityManagerInterface $manager, int $id)
    {
        $post = $manager->getRepository(Post::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                "il n'existe pas de categorie pour cet id ".$id
            );
        }

        if($request->isMethod('POST')){
            $title   = $request->request->get('title');
            $content = $request->request->get('content');
            $manager->remove($post);
            $manager->flush();
            $this->addFlash('success', 'Votre article a été bien supprimé');
            return $this->redirectToRoute('article_page');
        }
        return $this->render('article/delete-article.html.twig', ['post' => $post]);
    }


    #[Route('/article/read/{id}', name:'read_article')]
    public function read_article(Request $request, EntityManagerInterface $manager, int $id)
    {
        $post = $manager->getRepository(Post::class)->find($id);
        $post->setViewTimes($post->getViewTimes() + 1);
        $manager->flush();
        return $this->render('article/read-article.html.twig', ['post' => $post]);
    }


    #[Route('/article/like/{id}', name:'like_article')]
    public function like_article(Request $request, EntityManagerInterface $manager, int $id)
    {
        $post = $manager->getRepository(Post::class)->find($id);
        $post->setliked($post->getliked() + 1);
        $manager->flush();
        return $this->render('article/read-article.html.twig', ['post' => $post]);
    }
}