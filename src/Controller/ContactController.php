<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController{
    

    #[Route('/contacts', name:'contactpage')]
    public function contactPage()
    {
        return $this->render('contacts.html.twig');
    }
}