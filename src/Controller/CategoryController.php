<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Category; 
use Doctrine\ORM\EntityManagerInterface;

class CategoryController extends AbstractController
{

    #[Route('/category/list', name:'list_category')]
    public function list_category(EntityManagerInterface $manager)
    {
        $categories = $manager->getRepository(Category::class)->findAll();
        return $this->render('category/list-category.html.twig', ['categories' => $categories]);
    }


    #[Route('/category/add', name:'add_category')]
    public function add_category(Request $request, EntityManagerInterface $manager)
    { // injecton de dependances pour pouvoir utiliser manager et request
        
        if($request->isMethod('POST')){

            $name = $request->request->get('category'); 
            $category = new Category();
            $category->setName($name);

            $manager->persist($category);
            $manager->flush(); // s'occupe de l'ecriture en bdd
            $this->addFlash('success', 'Votre catégorie a été bien enregistrée');
            return $this->redirectToRoute('list_category');
        }

        return $this->render('category/add-category.html.twig');
        
    }


    #[Route('/category/update/{id}', name: 'update_category')]
    public function update_category(Request $request, EntityManagerInterface $manager, int $id): Response
    {
        $category = $manager->getRepository(Category::class)->find($id);

        if (!$category) {
            throw $this->createNotFoundException(
                "il n'existe pas de categorie pour cet id ".$id
            );
        }
        
        if($request->isMethod('POST')){
            
            $name = $request->request->get('category');
            $oldName = $category->getName();
            $category->setName($name);
            $manager->flush();
            $this->addFlash('success', 'Votre catégorie <strong>' .$oldName. '</strong> a été bien modifiée en :<strong>' .$category->getName(). '</strong>');
            return $this->redirectToRoute('list_category');

        }
        return $this->render('category/update-category.html.twig', ['category' => $category]);


    }


    #[Route('/category/delete/{id}', name: 'delete_category')]
    public function delete_category(Request $request, EntityManagerInterface $manager, int $id)
    {
        $category = $manager->getRepository(Category::class)->find($id);

        if (!$category) {
            throw $this->createNotFoundException(
                "il n'existe pas de categorie pour cet id ".$id
            );
        }

        if($request->isMethod('POST')){
            $name = $request->request->get('category');
            $manager->remove($category);
            $manager->flush();
            $this->addFlash('success', 'Votre catégorie <strong>' .$category->getName(). '</strong> a été bien supprimée ');
            return $this->redirectToRoute('list_category');
        }
        return $this->render('category/delete-category.html.twig', ['category' => $category]);
    }

       

}
